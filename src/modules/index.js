import params from '@/modules/params'
export default {
  // The install method will be called with the Vue constructor as the first argument, along with possible options
  install (Vue) {
    Vue.prototype.$params = params
  }
}
