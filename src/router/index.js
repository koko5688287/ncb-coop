import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Executive from '@/components/Executive/Executive'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/executive',
      name: 'Executive',
      component: Executive
    }
  ]
})
